package com.fb.appjc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.appjc.databinding.ActivityFirebaseBinding;
import com.example.appjc.databinding.ActivityPrimariaBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firestore.v1.Document;
import com.model.appjc.Producto;

import java.util.HashMap;
import java.util.Map;

public class FirebaseActivity extends AppCompatActivity {

    private ActivityFirebaseBinding binding;
    private FirebaseFirestore db;
    private CollectionReference crAmazon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFirebaseBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        db = FirebaseFirestore.getInstance();
        crAmazon = db.collection("productos-ad");
    }


    public void onClickCrearProducto(View view) {
        //crearConDiccionario();
        Producto p = new Producto(100,"Bicicleta",350.50,15,"https://m.media-amazon.com/images/I/81UFv-sLqxL._AC_UL320_.jpg");
        crearProducto(p);
    }

    //crear registros con pojo
    private void crearProducto(Producto p) {
        crAmazon
            .add(p)
            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                @Override
                public void onSuccess(DocumentReference documentReference) {
                    Log.d("FB","Producto agregado correctamente:" + documentReference.getId());
                }

            });
    }

    //crear registros con diccionarios
    private void crearConDiccionario() {
        Map<String,Object> producto = new HashMap<>();
        producto.put("codigo",101);
        producto.put("nombre","Laptop");
        producto.put("precio",1200.50);
        producto.put("cantidad",100);
        producto.put("foto","https://images-na.ssl-images-amazon.com/images/G/01/apparel/rcxgs/tile._CB483369110_.gif");

        crAmazon
                .add(producto)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("FB","Producto agregado correctamente:" + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("FB","Error producto no agregado",e);
                    }
                });
    }

    public void onClickConsultarProductos(View view) {
        crAmazon
            .get()
            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Log.d("FB", document.getId() +  "-->" + document.getData());
                        }
                    }
                }
            });
    }

    public void onClickConsultarProductoPorId(View view) {
        DocumentReference prodRef = crAmazon.document("JhPT755syjJE9k6Hz8In");
        prodRef
            .get()
            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Log.d("FB", document.getData().toString());
                        }
                    }
                }
            });

    }

    public void onClickModificar(View view) {
        Producto p = new Producto(100,"Bicicleta",333,33,"https://m.media-amazon.com/images/I/81UFv-sLqxL._AC_UL320_.jpg");
        String id = "itziHoq4bkuBqjCNovDa";
        modificarProducto(id,p);

        grabarProducto();

        //consultarComentarios("500");

        eliminarProducto("wlsVg99IhsBojlgyXyjb");
    }

    private void modificarProducto(String id, Producto p) {
        crAmazon.document(id).set(p);
    }

    private void eliminarProducto(String id) {

        crAmazon.document(id).delete();
    }

    private void grabarProducto() {
        Producto p =  new Producto(200,"Audifonos",150,33,"https://images-na.ssl-images-amazon.com/images/G/01/US-hq/2020/img/Consumer_Electronics/XCM_Manual_1226352_US_us_ce_gift_finder_3_3108253_500x236_2Xundefined.jpg");
        crAmazon.document("500" ).set(p);
    }


    private void consultarComentarios(String productoID) {
        CollectionReference crComentarios = db.collection("comentarios");
        Query query = crComentarios.whereEqualTo("producto",productoID).orderBy("fecha", Query.Direction.DESCENDING).limit(10);
        query
            .get()
            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Log.d("FB", document.getId() + "-->" + document.getData());
                        }
                    }
                }
            });
    }

    private void consultarPrecio(String productoID) {
        String precioPath = productoID + "/precio";
        crAmazon.document(precioPath)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                Log.d("FB", document.getData().toString());
                            }
                        }
                    }
                });
    }
}