package com.fb.appjc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.appjc.databinding.ActivityCatalogoBinding;
import com.example.appjc.databinding.ActivityFirebaseRecyclerBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.model.appjc.Producto;
import com.viewscomplex.appjc.AdaptadorProductos;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FirebaseRecyclerActivity extends AppCompatActivity {
    private ActivityFirebaseRecyclerBinding binding;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFirebaseRecyclerBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        db = FirebaseFirestore.getInstance();
        consultarListaProductos();
    }

    private void consultarListaProductos() {
        db.collection("productos")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Producto> productos = new ArrayList<>();
                            productos = task.getResult().toObjects(Producto.class);
                            /*//usando json a diccionarios
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Map<String,Object> md = document.getData();
                                int codigo = ((Long) md.get("codigo")).intValue();
                                String nombre = (String) md.get("nombre");
                                double precio = (Double) md.get("precio");
                                double cantidad = (Double)md.get("cantidad");
                                String foto = (String)md.get("foto");
                                productos.add(new Producto(codigo,nombre,precio,cantidad,foto));
                            }*/
                            cargarRecyclerView(productos);
                        }
                    }
                });
    }

    private void cargarRecyclerView(List<Producto> productos) {
        binding.recyclerViewProductos.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerViewProductos.setLayoutManager(layoutManager);

        RecyclerView.Adapter mAdapter = new AdaptadorProductos(productos);
        binding.recyclerViewProductos.setAdapter(mAdapter);
    }

}