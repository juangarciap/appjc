package com.fb.appjc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.appjc.databinding.ActivityCargarProductosBinding;
import com.example.appjc.databinding.ActivityFirebaseBinding;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.model.appjc.Producto;
import com.model.appjc.ProductosCsv;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CargarProductosActivity extends AppCompatActivity {

    private ActivityCargarProductosBinding binding;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCargarProductosBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        db = FirebaseFirestore.getInstance();
    }

    public void cargarDatosFirestorm(View view) throws IOException {
        ProductosCsv pcsv = new ProductosCsv();
        List<Producto> productos = new ArrayList<>();
        try {
            productos = pcsv.getListaProductos(getAssets().open("productos.csv"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        for (Producto producto : productos) {
            Map<String,Object> mapa = getMapa(producto);
            db.collection("amazon")
                    .add(mapa)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.d("FB","Producto amazon agregado correctamente:" + documentReference.getId());
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("FB","Error producto amazon no agregado",e);
                        }
                    });
        }
    }

    private Map<String,Object>  getMapa(Producto producto) {
        Map<String,Object> mapa = new HashMap<>();
        mapa.put("codigo",producto.getCodigo());
        mapa.put("nombre",producto.getNombre());
        mapa.put("precio",producto.getPrecio());
        mapa.put("cantidad",producto.getCantidad());
        mapa.put("foto",producto.getFoto());
        return mapa;
    }
}