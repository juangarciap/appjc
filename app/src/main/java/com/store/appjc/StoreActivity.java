package com.store.appjc;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.bumptech.glide.Glide;
import com.example.appjc.databinding.ActivityStoreBinding;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class StoreActivity extends AppCompatActivity {
    private ActivityStoreBinding binding;
    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityStoreBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        storageReference = FirebaseStorage.getInstance("gs://ug-2021c1.appspot.com").getReference();
    }

    private void descargarImagen() {
        storageReference.child("catalogo/ibuprofeno.jpg")
                .getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Glide.with(getApplicationContext()).load(uri).into(binding.imageViewFirestorage);
                    }
                });
    }


    public void mostrarImagen(View view) {
        descargarImagen();
    }
}