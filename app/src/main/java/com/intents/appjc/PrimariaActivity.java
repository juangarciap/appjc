package com.intents.appjc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.appjc.R;
import com.example.appjc.databinding.ActivityPrimariaBinding;

import java.util.Calendar;
import java.util.Date;

public class PrimariaActivity extends AppCompatActivity {

    private ActivityPrimariaBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setContentView(R.layout.activity_primaria);
        //EditText editTextCorreo = (EditText) findViewById(R.id.editTextTextEmailAddress);

        binding = ActivityPrimariaBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
    }


    public void onClickIrActividad(View view) {
        //Explicito
        Intent intent = new Intent(this,SecundariaActivity.class);
        String email = binding.editTextTextEmailAddress.getText().toString();
        intent.putExtra(Intent.EXTRA_TEXT,email);
        startActivity(intent);
    }


    public void onClickBotonHora(View view) {
        Date horaActual = Calendar.getInstance().getTime();
        Toast.makeText(this, horaActual.toString(), Toast.LENGTH_SHORT).show();

        //implícito
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://www.ug.edu.ec/"));
        startActivity(intent);

    }
}