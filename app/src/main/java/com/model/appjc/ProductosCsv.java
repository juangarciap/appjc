package com.model.appjc;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ProductosCsv {

    public List<Producto> getListaProductos(InputStream stream) {
        List<Producto> productos = new ArrayList<>() ;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String line;
            Log.e("Reader Stuff",reader.readLine());
            while ((line = reader.readLine()) != null) {
                Log.e("code",line);
                String[] d = line.split(";");

                Producto p = new Producto(Integer.parseInt(d[0]),d[1],Double.parseDouble(d[2]),Double.parseDouble(d[3]),d[4]);
                productos.add(p);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return productos;
    }
}
