package com.sql;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;

import com.example.appjc.databinding.ActivitySqlBinding;

public class SqlActivity extends AppCompatActivity {
    private ActivitySqlBinding binding;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySqlBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        db = openOrCreateDatabase("facturacion",MODE_PRIVATE,null);
    }


    private void crearDB() {
        db.execSQL("CREATE TABLE IF NOT EXISTS productos(codigo INT, nombre VARCHAR, precio DOUBLE)");
        db.execSQL("CREATE TABLE IF NOT EXISTS clientes(codigo INT, cedula VARCHAR, nombre VARCHAR)");
        db.execSQL("CREATE TABLE IF NOT EXISTS factura_cab(codigo_cli INT, fecha DATE)");
        db.execSQL("INSERT INTO productos (nombre,precio) VALUES('x',2)");
    }

}